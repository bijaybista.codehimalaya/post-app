<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


// Route::get('/test','WelcomeController@abc');

// Route::post('add-data','WelcomeController@add')->name('add-data');

// Route::get('viewdata','WelcomeController@data')->name('viewdata');


//  Route::resource('post','PostController');

 Route::resource('demo','DemoController');


//Route::post('addpost','PostController@store');

//  Route::resource('post', 'PostController');
