<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Post;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $abc = Post::all();
      return view('post.index',compact('abc'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      return view('post.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $post = new Post(); //Object Create (Model Define)
        $data = $request->all(); //storing all values in data variable
        $rules = [      //rules define 
            'title' => 'required',
            'description' => 'required' 
        ];
        $message = [     //validation message
            'title.required' => 'Please Enter Title',
            'description.required' => 'Please Enter description'
        ];

        $this->validate($request,$rules,$message);  
        $post->title = $data['title'];
        $post->description = $data['description'];
        $post->save();
        return redirect()->back()->with('success','You Have Successfully Inserted'); 

        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       $a = Post::findorfail($id);  //Select * from post where id = '1'
       return view('post.edit',compact('a'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $ab = Post::findorfail($id);
        $data = $request->all();
        $ab->title = $data['title'];
        $ab->description = $data['description'];
        $ab->save();

        return redirect()->route('post.index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       $abc = Post::findorfail($id);
       $abc->delete();
       return redirect()->route('post.index'); //
    }
}
