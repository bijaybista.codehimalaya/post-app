<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Test;   //model path 

class WelcomeController extends Controller
{
    public function abc(){

        return view('test');
    }

    public function data(){
        $abc = Test::all();
        return view('viewdata',compact('abc'));
    }

    public function add(Request $request){
       $data  = $request->all(); 
       $abc = new Test; 
        $abc->title = $data['title'];
        $abc->details = $data['details'];
        $abc->save();
        return view('test')->with('success','You have succesfully added');

    }


}
