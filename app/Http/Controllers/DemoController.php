<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Abc;

class DemoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $data = Abc::all();
       return view('demo.view',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       return view('demo.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       $bc = new Abc;
       $data = $request->all();
       $bc->firstname = $data['firstname'];
       $bc->lastname = $data['lastname'];
       $bc->save();
       
       return redirect()->route('demo.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       $data = Abc::findorfail($id);
       return view('demo.edit',compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       $bc = Abc::findorfail($id);
       $data = $request->all();
       $bc->firstname = $data['firstname'];
       $bc->lastname = $data['lastname'];
       $bc->save();

       return redirect()->route('demo.index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Abc::findorfail($id);
        $data->delete();

        return redirect()->back();
    }
}
