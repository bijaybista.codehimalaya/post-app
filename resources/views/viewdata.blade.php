<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>View Data</title>
</head>
<body>
    <h2>View data</h2>

    <table>
        <tr>
            <td>Title</td>
            <td>Details</td>
             <td>Action</td>
        </tr>
        @foreach($abc as $test)
        <tr>
            <td>{{$test->title}}</td>
            <td>{{$test->details}}</td>
            <td><a href="{{route('post.edit',$test->id)}}">Edit</a></td>
        </tr>
        @endforeach
    </table>
</body>
</html>