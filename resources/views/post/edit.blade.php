<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Post Add</title>
</head>
<body>

    <h1>Edit Post</h1>
    <form action="{{route('post.update',$a->id)}}" method="post"> 
        <!-- compact a -->
        @csrf
        @method('Patch')
        <p>Title : <input type="text" name="title" value="{{$a->title}}" ></p>
        <p>Description : <textarea name="description" >{{$a->description}}</textarea></p>
        <p><button type="submit">Submit</button></p>
    </form>
</body>
</html>