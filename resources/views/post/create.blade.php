<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Post Add</title>
</head>
<body>

@if(Session::has('success'))
   {{Session::get('success')}}
@endif
@if($errors->any())
                            <div class="alert alert-danger">
                                <ul style="list-style:none;">
                                  @foreach($errors->all() as $error)
                                    <li>{{$error}}</li>
                                  @endforeach
                                </ul>
                            </div>
                        @endif
    <h1>Add New Post</h1>
    <form action="{{route('post.store')}}" method="post">
        @csrf
        <p>Title : <input type="text" name="title" ></p>
        <p>Description : <textarea name="description"></textarea></p>
        <button type="submit">Submit</button>
    </form>
</body>
</html>