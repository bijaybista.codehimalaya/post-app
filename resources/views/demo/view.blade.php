<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>

<a href="{{route('demo.create')}}">Add New</a>
    <table>
        <tr>
            <td>First Name</td>
            <td>Last Name</td>
            <td>Action</td>
        </tr>

        @foreach($data as $abc)
        <tr>
            <td>{{$abc->firstname}}</td>
            <td>{{$abc->lastname}}</td>
            <td><a href="{{route('demo.edit',$abc->id)}}">Edit</a> || 
                <form action="{{route('demo.destroy',$abc->id)}}" method="post" onsubmit="return confirm('Are you Sure')">
                @method('delete')
                @csrf   
                <button type="submit">  Delete </button>
                </form>
        </td>
        </tr>
        @endforeach

    </table>
</body>
</html>