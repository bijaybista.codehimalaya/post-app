<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <form action="{{route('demo.update',$data->id)}}" method="post">
        @csrf
        @method("Patch")    
    <p>First Name : <input type="text" name="firstname" id="" value="{{$data->firstname}}"></p>
    <p>Last Name: <input type="text" name="lastname" id="" value = "{{$data->lastname}}"></p>
    <button type="submit">Update</button>
    </form>
</body>
</html>