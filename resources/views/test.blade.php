<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Test</title>
</head>
<body>
    <h2>Test Form</h2>

    <form action="{{route('add-data')}}" method="post">
        @csrf
        <p> Title : <input type="text" name="title" > </p>
        <p> Details : <input type="text" name="details" ></p>

        <p><button type="submit">Add</button></p>
    </form>


</body>
</html>